# zum aufrufen von youtube-dl
import subprocess

url_file_location = r"urls.txt" # same directory, else whole path C: ....

# config path(C:\Users\<name>\youtube-dl.conf):


# open file in read-mode and name
with open(url_file_location, "r") as document:
    # read all lines
    lines = document.readlines()
    # go through each url
    for url in lines:
        # Neuen subprozess öffnen und youtube-dl ausführen
        # mit system-config
        #subprocess.call(['youtube-dl', url])
        # ignore config & extract audio
        subprocess.call(['youtube-dl','--ignore-config' ,'-x' ,"--audio-format", "mp3", "--write-thumbnail" ,url])
        # .call(...) lässt nur 1 dl gleichzeitg zu. .Popen(...) lässt mehrere zu.
        # Kannst selber speed vs auslastung ausprobieren 
document.close()

# überschreibe urls.txt mit leerer datei
open(url_file_location, 'w').close()
# oeffne explorer um fiels gleich zu verschieben
subprocess.call(['explorer.exe', '.'])
